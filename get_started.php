<!DOCTYPE html>
<html>
<head>
  <title>Log In</title>
  <link rel="stylesheet" type="text/css" href="login.css">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <link href="https://fonts.googleapis.com/css?family=Raleway:100" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>
  <center>
    <div class="login_div">
      <p id="header_para" class="w3-animate-fading"><b>REGISTER TO THE CLASSROOM</b><span id="card"> &nbsp ORGANISED BY RISHABH</span></p>
      <hr id="hr1" />&nbsp
      <hr id="hr2" /><br /><br /><br />
      <form action="targetLogin.php" method="post">
        <input type="text" name="username" placeholder="username" required class="input_style w3-animate-right"><br /><br />
        <input type="password" name="password" placeholder="password" required class="input_style w3-animate-left"><br /><br />
        <input type="text" name="addressl1" placeholder="address line 1" required class="input_style w3-animate-left"><br /><br />
        <input type="text" name="addressl2" placeholder="address line 2" required class="input_style w3-animate-left"><br /><br />
        <button style="width:200px;border-radius:15px;height:60px;" type="button" class="btn btn-default">PLEASE PAY TO PROCEED</button><br><br>
        <input style="border-radius:15px;height:60px;" type="submit" name="submit_button"  disabled value="REGISTER" id="login_button" class="w3-animate-bottom"><br />
      </form>
 
    </div>
  </center>
</body>

</html>