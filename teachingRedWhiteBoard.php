 <!DOCTYPE html>
 <html>
 <head>
 	<title>Teaching Mode-(Prototype Version 1)</title>
 	<link href="https://fonts.googleapis.com/css?family=Sigmar+One&display=swap" rel="stylesheet">
 <script src="https://kit.fontawesome.com/5076bb93ac.js" crossorigin="anonymous"></script>
 	<script type="text/javascript" src="sidebar.js"></script>
 	<script src="https://code.jquery.com/jquery-3.3.1.min.js" 
        integrity="sha384-tsQFqpEReu7ZLhBV2VZlAu7zcOV+rXbYlF2cqB8txI/8aZajjp4Bqd+V6D5IgvKT" 
        crossorigin="anonymous">
	</script> 
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
	<script type="text/javascript" src="whiteboardRed.js"></script>
<!-- 	<script type="text/javascript" src="sendCanvas.js" ></script>
 --> 	<link rel="stylesheet" type="text/css" href="main.css">
 </head>
 <body>

<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <a href="#">About</a>
  <a href="#">Services</a>
  <a href="#">Clients</a>
  <a href="#">Contact</a>
</div>

 
<div id="main">
<!-- Use any element to open the sidenav -->
<i onclick="openNav()" id="hamMenuIcon" class="fas fa-bars"></i>
	 <div id="menuTab">
	 	<h2 >Teaching Arena</h2>
	 </div>
	 <div id="bodyTab">
	 	<div id="mainScreen">
  			<div class="container" id="canvasWhite">
    		<canvas id="myCanvas">
				Sorry, your browser does not support HTML5 canvas technology.
			</canvas>
  			</div>
		</div>
		<div id="primaryScreen">
 <div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v6.0"></script>

 <div class="fb-video" data-href="https://www.facebook.com/rishabh.rana.5203577/videos/520670792205964/" data-width="500" data-show-text="false"><blockquote cite="https://developers.facebook.com/rishabh.rana.5203577/videos/520670792205964/" class="fb-xfbml-parse-ignore"><a href="https://developers.facebook.com/rishabh.rana.5203577/videos/520670792205964/"></a><p>HACKATHON IXI LIVE STREAM</p>Posted by <a href="https://www.facebook.com/rishabh.rana.5203577">Rishabh Rana</a> on Monday, February 3, 2020</blockquote></div>
            
    </div>
    <div id="colorpallete">
      <div class="btn-group mr-2" role="group" aria-label="First group">
         <form action="teachingBlueWhiteBoard.php">
              <input class="btn btn-primary" type="submit" value="BluePen" />
        </form>
        <form action="#">
              <input class="btn btn-danger" type="submit" value="RedPen" />
        </form>
        
        <form action="teachingBlackWhiteBoard.php">
              <input class="btn btn-dark" type="submit" value="BlackPen" />
        </form>
        
        <form action="teachingGreenWhiteBoard.php">
              <input class="btn btn-info" type="submit" value="GreenPen" />
        </form>
  </div>
    </div>

  			</div>
		</div>
		
	</div>

	</div>
	
 <script type="text/javascript" src="whiteboardSend.js" ></script>
 <script type="text/javascript" src="teachingMsg.js"></script>

<script type="text/javascript">
 		$num = $('.my-card').length;
		$even = $num / 2;
		$odd = ($num + 1) / 2;

	if ($num % 2 == 0) {
  		$('.my-card:nth-child(' + $even + ')').addClass('active');
  		$('.my-card:nth-child(' + $even + ')').prev().addClass('prev');
  		$('.my-card:nth-child(' + $even + ')').next().addClass('next');
	} else {
  		$('.my-card:nth-child(' + $odd + ')').addClass('active');
  		$('.my-card:nth-child(' + $odd + ')').prev().addClass('prev');
  		$('.my-card:nth-child(' + $odd + ')').next().addClass('next');
	}

	$('.my-card').click(function() {
  	$slide = $('.active').width();
  	console.log($('.active').position().left);
  
  	if ($(this).hasClass('next')) {
    	$('.card-carousel').stop(false, true).animate({left: '-=' + $slide});
  	} else if ($(this).hasClass('prev')) {
    	$('.card-carousel').stop(false, true).animate({left: '+=' + $slide});
  	}
  
  	$(this).removeClass('prev next');
  	$(this).siblings().removeClass('prev active next');
  
  	$(this).addClass('active');
  	$(this).prev().addClass('prev');
  	$(this).next().addClass('next');
});


	// Keyboard nav
	$('html body').keydown(function(e) {
 	 if (e.keyCode == 37) { // left
    	$('.active').prev().trigger('click');
  	}
  	else if (e.keyCode == 39) { // right
    	$('.active').next().trigger('click');
  	}
});


 	</script>
 </body>
 </html>