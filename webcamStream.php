<?php
   
$fp = fopen('activeUser.txt', 'r');
$s = fread($fp,filesize("activeUser.txt"));
fclose($fp);

?>
 <!DOCTYPE html>
 <html>
 <head>
 	<title>Teaching Mode-(Prototype Version 1)</title>
 	<link href="https://fonts.googleapis.com/css?family=Sigmar+One&display=swap" rel="stylesheet">
 <script src="https://kit.fontawesome.com/5076bb93ac.js" crossorigin="anonymous"></script>
 	<script type="text/javascript" src="sidebar.js"></script>
 	<script src="https://code.jquery.com/jquery-3.3.1.min.js" 
        integrity="sha384-tsQFqpEReu7ZLhBV2VZlAu7zcOV+rXbYlF2cqB8txI/8aZajjp4Bqd+V6D5IgvKT" 
        crossorigin="anonymous">
	</script> 
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
	<script type="text/javascript" src="whiteboard.js"></script>
<!-- 	<script type="text/javascript" src="sendCanvas.js" ></script>
 --> 	<link rel="stylesheet" type="text/css" href="main.css">
 <script type="text/javascript" src="webcamStream.js"></script>
 </head>
 <body>
   <div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v6.0"></script>
<script type="text/javascript">
  function blue(){
    var myCanvas = document.getElementById("myCanvas");
    var ctx = myCanvas.getContext("2d");
    ctx.strokeStyle = "blue";
    ctx.stroke();
  }
  function green(){
    var myCanvas = document.getElementById("myCanvas");
    var ctx = myCanvas.getContext("2d");
    ctx.strokeStyle = "green";
    ctx.stroke();
  }
  function red(){
    var myCanvas = document.getElementById("myCanvas");
    var ctx = myCanvas.getContext("2d");
    ctx.strokeStyle = "red";
    ctx.stroke();
  }
  function black(){
    var myCanvas = document.getElementById("myCanvas");
    var ctx = myCanvas.getContext("2d");
    ctx.strokeStyle = "black";
    ctx.stroke();
  }
  function white(){
    var myCanvas = document.getElementById("myCanvas");
    var ctx = myCanvas.getContext("2d");
    ctx.strokeStyle = "white";
    ctx.stroke();
  }
</script>

<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <a href="#">About</a>
  <a href="#">Services</a>
  <a href="#">Clients</a>
  <a href="#">Contact</a>
</div>

 
<div id="main">
<!-- Use any element to open the sidenav -->
<i onclick="openNav()" id="hamMenuIcon" class="fas fa-bars"></i>
	 <div id="menuTab">
	 	<h2 >Teaching Arena</h2>
	 </div>
	 <div id="bodyTab">
	 	<div id="mainScreen" style="width:1150px;height:600px;">
  			<div class="containers" id="canvasWhite">
           
      <div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v6.0"></script>

 <div class="fb-video" data-href="https://www.facebook.com/rishabh.rana.5203577/videos/520670792205964/" data-height="1150" data-width="1150" data-show-text="false"><blockquote cite="https://developers.facebook.com/rishabh.rana.5203577/videos/520670792205964/" class="fb-xfbml-parse-ignore"><a href="https://developers.facebook.com/rishabh.rana.5203577/videos/520670792205964/"></a><p>HACKATHON IXI LIVE STREAM</p>Posted by <a href="https://www.facebook.com/rishabh.rana.5203577">Rishabh Rana</a> on Monday, February 3, 2020</blockquote></div>

    		</div>

</div>
     
		</div>
		<div style="height: 600px;" id="primaryScreen">
 <div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v6.0"></script>

 <div class="fb-video" data-href="https://www.facebook.com/rishabh.rana.5203577/videos/520670792205964/" data-width="500" data-show-text="false"><blockquote cite="https://developers.facebook.com/rishabh.rana.5203577/videos/520670792205964/" class="fb-xfbml-parse-ignore"><a href="https://developers.facebook.com/rishabh.rana.5203577/videos/520670792205964/"></a><p>HACKATHON IXI LIVE STREAM</p>Posted by <a href="https://www.facebook.com/rishabh.rana.5203577">Rishabh Rana</a> on Monday, February 3, 2020</blockquote></div>
<div id="toolbar" style="width: 600px;text-align:center;margin-top:30px;">
  <button class="btn btn-dark" style="margin-left:-150px;width: 540px;" onclick="window.location.href='webcamStream.php' ">WebCam Streaming Mode </button><br>
  <button class="btn btn-dark" style="margin-left:-150px;width: 540px;" onclick="">Chat Room Mode</button><br>
  <button class="btn btn-dark" style="margin-left:-150px;width: 540px;" onclick="window.location.href='teachingWhiteBoard.php'">WhiteBoard Mode </button><br>
  <button class="btn btn-dark" style="margin-left:-150px;width: 540px;" onclick="caMode()">Code Arena Mode</button><br>
  <button class="btn btn-dark" style="margin-left:-150px;width: 540px;" onclick="window.location.href='sendFiles.php'">Send Files</button><br>
  <button class="btn btn-dark" style="margin-left:-150px;width: 540px;" onclick="afMode()">Add Files</button><br>
  <button class="btn btn-dark" style="margin-left:-150px;width: 540px;" onclick="doubtMode()">Doubts</button><br>
</div>
   
    </div>
    
  			</div>
		</div>
		
	</div>

	</div>
	
 <script type="text/javascript" src="whiteboardSend.js" ></script>
 <script type="text/javascript" src="teachingMsg.js"></script>
 <script type="text/javascript">
   fx();
   function fx(){
    var active = <?php echo $s ?> ;
    if(active==0){
    var text = prompt("Please Enter Your facebook link to continue","no link");
    var ajax = new XMLHttpRequest();
    ajax.open("GET", "scriptToFB.php?text="+text, true);
    ajax.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    ajax.send("text="+text);
    document.getElementsByClassName("fb-video")[0].setAttribute("data-href", text);
        document.getElementsByClassName("fb-video")[1].setAttribute("data-href", text);


    }
    else{
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        document.getElementsByClassName("fb-video")[0].setAttribute("data-href", this.responseText);
        document.getElementsByClassName("fb-video")[0].setAttribute("data-href", this.responseText);
      

      }
    };
    xmlhttp.open("GET", "fbLink.txt");
    xmlhttp.send();
    }
   }

 </script>

<script type="text/javascript">
 		$num = $('.my-card').length;
		$even = $num / 2;
		$odd = ($num + 1) / 2;

	if ($num % 2 == 0) {
  		$('.my-card:nth-child(' + $even + ')').addClass('active');
  		$('.my-card:nth-child(' + $even + ')').prev().addClass('prev');
  		$('.my-card:nth-child(' + $even + ')').next().addClass('next');
	} else {
  		$('.my-card:nth-child(' + $odd + ')').addClass('active');
  		$('.my-card:nth-child(' + $odd + ')').prev().addClass('prev');
  		$('.my-card:nth-child(' + $odd + ')').next().addClass('next');
	}

	$('.my-card').click(function() {
  	$slide = $('.active').width();
  	console.log($('.active').position().left);
  
  	if ($(this).hasClass('next')) {
    	$('.card-carousel').stop(false, true).animate({left: '-=' + $slide});
  	} else if ($(this).hasClass('prev')) {
    	$('.card-carousel').stop(false, true).animate({left: '+=' + $slide});
  	}
  
  	$(this).removeClass('prev next');
  	$(this).siblings().removeClass('prev active next');
  
  	$(this).addClass('active');
  	$(this).prev().addClass('prev');
  	$(this).next().addClass('next');
});


	// Keyboard nav
	$('html body').keydown(function(e) {
 	 if (e.keyCode == 37) { // left
    	$('.active').prev().trigger('click');
  	}
  	else if (e.keyCode == 39) { // right
    	$('.active').next().trigger('click');
  	}
});


 	</script>
 </body>
 </html>